package com.pms.sdk.push;

import android.content.Context;
import android.os.AsyncTask;

import com.google.firebase.iid.FirebaseInstanceId;
import com.pms.sdk.IPMSConsts;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;

import java.io.IOException;
import java.util.List;

//2019.01.10 손보광 비동기로 PushToken 요청, FCMPushService onNewToken 발급 빈도가 너무 낮아서 만듬
public class FCMRequestToken extends AsyncTask<String, FCMRequestToken.Result, FCMRequestToken.Result>
{
    private Context context;
    private String fcmSenderId;
    private List<String> fcmSenderIdList;
    private Callback callback;

    public FCMRequestToken(Context context, String fcmSenderId, Callback callback)
    {
        this.context = context;
        this.fcmSenderId = fcmSenderId;
        this.callback = callback;
    }

    public FCMRequestToken(Context context, List<String> fcmSenderIdList, Callback callback)
    {
        this.context = context;
        this.fcmSenderIdList = fcmSenderIdList;
        this.callback = callback;
    }

    @Override
    protected Result doInBackground(String... strings)
    {
        Result result = new Result();
        String token = null;
        String message = null;
        String senderId = "";
        if(fcmSenderId!=null)
        {
            senderId = fcmSenderId;
        }
        else
        {
            if(fcmSenderIdList!=null)
            {
                StringBuilder builder = new StringBuilder();
                for(int i=0; i<fcmSenderIdList.size(); i++)
                {
                    builder.append(fcmSenderIdList.get(i));
                    if(i != (fcmSenderIdList.size()-1))
                    {
                        builder.append(",");
                    }
                }
                senderId = builder.toString();
            }
        }

        try
        {
            token = FirebaseInstanceId.getInstance().getToken(senderId, "FCM");
        }
        catch (Exception e)
        {
            message = e.getMessage();
            CLog.e(e.getMessage());
        }
        if(token != null)
        {
            result.setSuccess(true);
            result.setMessage(token);
            DataKeyUtil.setDBKey(context, IPMSConsts.DB_GCM_TOKEN, token);
        }
        else
        {
            result.setSuccess(false);
            result.setMessage(message);
        }

        return result;
    }

    @Override
    protected void onPostExecute(Result result)
    {
        super.onPostExecute(result);
        if(callback!=null)
        {
            callback.callback(result.isSuccess(), result.getMessage());
        }
    }

    public interface Callback
    {
        public void callback(boolean isSuccess, String message);
    }
    public static class Result
    {
        private boolean isSuccess;
        private String message;

        public Result() { }

        public boolean isSuccess()
        {
            return isSuccess;
        }

        public void setSuccess(boolean success)
        {
            isSuccess = success;
        }

        public String getMessage()
        {
            return message;
        }

        public void setMessage(String message)
        {
            this.message = message;
        }
    }
}

package com.pms.sdk.push.mqtt;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DateUtil;

import java.util.Calendar;

/**
 * @author haewon
 */
public class MQTTScheduler
{
    private static MQTTScheduler instance;

    private static final int SCHEDULE_ALARM_UNIQUE_ID = 901232;

    /**
     * 배포 시, 유의점
     * DEFAULT_SCHEDULE_TIME : 1분 고정
     */
    public static final long DEFAULT_SCHEDULE_TIME = 1000 * 60 * 1;

    public AlarmManager privateAlarm;

    private MQTTScheduler() { }

    public static MQTTScheduler getInstance()
    {
        if(instance == null)
        {
            instance = new MQTTScheduler();
        }

        return instance;
    }

    public void setSchedule(Context ctx)
    {
        //AlarmManager
        long nextAlarmInMilliseconds = Calendar.getInstance().getTimeInMillis() + DEFAULT_SCHEDULE_TIME;
        CLog.d( "AlarmManager Schedule next alarm at " + DateUtil.getMillisecondsToDate(nextAlarmInMilliseconds));

        int ver = Build.VERSION.SDK_INT;
        CLog.i("Device os version: " + ver + ", Release version: " + Build.VERSION.RELEASE);
        try
        {
            cancelSchedule(ctx);
        }
        catch (Exception e)
        {
            CLog.e(e.getMessage());
        }

    }

    private void cancelSchedule(Context ctx)
    {
        CLog.d("cancelSchedule init..");
        if (privateAlarm == null)
        {
            privateAlarm = (AlarmManager) ctx.getSystemService(Service.ALARM_SERVICE);
        }

        try
        {
            Intent intent = new Intent(ctx, RestartReceiver.class);
            intent.setAction(MQTTService.ACTION_RESTART);

            PendingIntent registerAlarm = PendingIntent.getBroadcast(ctx, SCHEDULE_ALARM_UNIQUE_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            privateAlarm.cancel(registerAlarm);
            registerAlarm.cancel();

        }
        catch (Exception e)
        {
            CLog.e("[ cancelSchedule ] exception: " + e.getMessage());
        }
    }

    private PendingIntent makePendingIntent (Context context)
    {
        Intent intent = new Intent(context, RestartReceiver.class);
        intent.setAction(MQTTService.ACTION_RESTART);

        return PendingIntent.getBroadcast(context, SCHEDULE_ALARM_UNIQUE_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }
}
package com.pms.sdk.api.request;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.api.APIManager.APICallback;

public class RealtimePush extends BaseRequest {

	public RealtimePush(Context context) {
		super(context);
	}

	/**
	 * get param
	 * 
	 * @return
	 */
	public JSONObject getParam (String bizId, String uuid, String pushTitle, String pushMsg, String popupContent, String inappContent, String infoCp,
			String map1, String pushValue, String pushImg, String msgType, String smsSendFlag) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("bizId", bizId);
			jobj.put("uuid", uuid);
			jobj.put("pushTitle", pushTitle);
			jobj.put("pushMsg", pushMsg);
			jobj.put("popupContent", popupContent);
			jobj.put("inappContent", inappContent);
			jobj.put("infoCp", infoCp);
			jobj.put("map1", map1);
			jobj.put("pushValue", pushValue);
			jobj.put("pushImg", pushImg);
			jobj.put("msgType", msgType);
			jobj.put("smsSendFlag", smsSendFlag);

			return jobj;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param bizId
	 * @param uuid
	 * @param pushTitle
	 * @param pushmMsg
	 * @param popupContent
	 * @param inappContent
	 * @param infoCp
	 * @param map1
	 * @param pushValue
	 * @param pushImg
	 * @param smsSendFlag
	 * @param apiCallback
	 */
	public void request (String bizId, String uuid, String pushTitle, String pushMsg, String popupContent, String inappContent, String infoCp,
			String map1, String pushValue, String pushImg, String msgType, String smsSendFlag, final APICallback apiCallback) {
		try {

			apiManager.call(API_REALTIME_PUSH,
					getParam(bizId, uuid, pushTitle, pushMsg, popupContent, inappContent, infoCp, map1, pushValue, pushImg, msgType, smsSendFlag),
					new APICallback() {
						@Override
						public void response (String code, JSONObject json) {
							if (CODE_SUCCESS.equals(code)) {
								requiredResultProc(json);
							}
							if (apiCallback != null) {
								apiCallback.response(code, json);
							}
						}
					});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		return true;
	}
}

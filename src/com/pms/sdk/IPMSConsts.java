package com.pms.sdk;

public interface IPMSConsts {

	public static final String PMS_VERSION = "3.0.3";

	public final static String PMS_PROPERY_NAME = "pms.properties";

	// [start] GCM
//	public static final String ACTION_REGISTRATION = "com.google.android.c2dm.intent.REGISTRATION";
	public static final String ACTION_RECEIVE = "com.google.android.fcm.intent.RECEIVE";
	// [end] GCM

	// [start] MQTT
	public static final String ACTION_START = "MQTT.START";
	public static final String ACTION_RESTART = "MQTT.RESTART";
	public static final String ACTION_FORCE_START = "MQTT.FORCE_START";
	public static final String ACTION_STOP = "MQTT.STOP";
	public static final String ACTION_RECONNECT = "MQTT.RECONNECT";
	public static final String ACTION_CHANGERECONNECT = "MQTT.CHANGERECONNECT";
	public static final String ACTION_USER_PRESENT = "android.intent.action.USER_PRESENT";
	public static final String ACTION_BOOT_COMPLETED = "android.intent.action.BOOT_COMPLETED";
	public static final String ACTION_ACTION_PACKAGE_RESTARTED = "android.intent.action.ACTION_PACKAGE_RESTARTED";
	// [end] MQTT

	// [start] PUSH
	public static final String KEY_MSG_ID = "i";
	public static final String KEY_NOTI_TITLE = "notiTitle";
	public static final String KEY_NOTI_MSG = "notiMsg";
	public static final String KEY_NOTI_IMG = "notiImg";
	public static final String KEY_MSG = "message";
	public static final String KEY_SOUND = "sound";
	public static final String KEY_MSG_TYPE = "t";
	public static final String KEY_DATA = "d";
	// [end] PUSH

	// [start] Receiver
	public static final String RECEIVER_PUSH = "com.pms.sdk.push";
	public static final String RECEIVER_CHANGE_POPUP = "receiver_popup_change";
	// [end] Receiver

	// [start] Preferences
	public static final String PREF_FILE_NAME = "pref_pms";

	public static final String FLAG_Y = "Y";
	public static final String FLAG_N = "N";

	public static final String PROTOCOL_SSL = "S";
	public static final String PROTOCOL_TCP = "T";

	public static final String LICENSE_ACTIVITY = "A";
	public static final String LICENSE_PENDING = "P";
	public static final String LICENSE_EXPIRED = "E";

	public static final String PREF_CLICK_LIST = "click_list";
	public static final String PREF_READ_LIST = "read_list";

	public static final String PREF_APP_USER_ID = "pref_app_user_id";
	public static final String PREF_ENC_KEY = "pref_enc_key";
	public static final String PREF_DEVICECERT_STATUS = "pref_devicecert_status";
	// [end] Preferences

	// [start] Popup Data Key
	public static final String POPUP_ACTIVITY = "popup_activity";

	// Popup Layout Data Key
	public static final String POPUP_BACKGROUND_RES_ID = "popup_background_res_id";
	public static final String POPUP_BACKGROUND_COLOR = "popup_background_color";
	public static final String PREF_WEB_LINK_URL = "pref_web_link_url";

	// Top Layout Data Key
	public static final String TOP_LAYOUT_FLAG = "top_layout_flag";
	public static final String TOP_BACKGROUND_RES_ID = "top_background_res_id";
	public static final String TOP_BACKGROUND_COLOR = "top_background_color";
	public static final String TOP_TITLE_TYEP = "top_title_type";
	public static final String TOP_TITLE_TEXT = "text";
	public static final String TOP_TITLE_IMAGE = "image";
	public static final String TOP_TITLE_RES_ID = "top_title_res_id";
	public static final String TOP_TITLE_TEXT_DATA = "top_title_text_data";
	public static final String TOP_TITLE_TEXT_COLOR = "top_text_color";
	public static final String TOP_TITLE_SIZE = "top_text_size";

	// Content Layout Data Key
	public static final String CONTENT_BACKGROUND_RES_ID = "content_background_res_id";
	public static final String CONTENT_BACKGROUND_COLOR = "content_background_color";
	public static final String CONTENT_TEXT_COLOR = "content_text_color";
	public static final String CONTENT_TEXT_SIZE = "content_text_size";

	// Bottom Layout Data Key
	public static final String BOTTOM_TEXT_BTN_COUNT = "bottom_text_btn_count";
	public static final String BOTTOM_RICH_BTN_COUNT = "bottom_rich_btn_count";
	public static final String BOTTOM_BACKGROUND_RES_ID = "bottom_Background_res_id";
	public static final String BOTTOM_BACKGROUND_COLOR = "bottom_Background_color";
	public static final String BOTTOM_BTN_RES_ID = "bottom_btn_res_id";
	public static final String BOTTOM_BTN_TEXT_STATE = "bottom_btn_text_state";
	public static final String BOTTOM_BTN_TEXT_NAME = "bottom_btn_text_name";
	public static final String BOTTOM_BTN_TEXT_COLOR = "bottom_btn_text_color";
	public static final String BOTTOM_BTN_TEXT_SIZE = "bottom_btn_text_size";
	public static final String BOTTOM_BTN_TEXT_CLICKLISTENER = "bottom_btn_text_clickListener";
	public static final String BOTTOM_BTN_RICH_CLICKLISTENER = "bottom_btn_rich_clickListener";
	public static final String WEB_LINK_RICH_TOUCHLISTENER = "web_link_rich_touchListener";

	// XML Layout Data Key
	public static final String XML_LAYOUT_TEXT_RES_ID = "layout_text_res_id";
	public static final String XML_LAYOUT_RICH_RES_ID = "layout_rich_res_id";
	public static final String XML_TEXT_BUTTON_TYPE = "xml_text_button_type";
	public static final String XML_RICH_BUTTON_TYPE = "xml_rich_button_type";
	public static final String XML_TEXT_BUTTON_TAG_NAME = "xml_text_button_tag_name";
	public static final String XML_RICH_BUTTON_TAG_NAME = "xml_rich_button_tag_name";
	public static final String XML_AND_DEFAULT_FLAG = "xml_and_default_flag";

	public static final int TEXT_DEFAULT_SIZE = 15;
	public static final int TITLE_TEXT_DEFAULT_SIZE = 25;
	public static final int TEXTVIEW = 1;
	public static final int LINEARLAYOUT = 2;
	public static final int WEBVIEW = 3;
	public static final int PROGRESSBAR = 4;
	public static final int IMAGEVIEW = 5;
	public static final int BUTTON_VIEW = 6;
	public static final int IMAGE_BUTTON = 7;
	public static final int RELATIVELAYOUT = 8;
	// [end] Popup Data Key

	// [start] api
	// default encrypt key
	public static final String DEFAULT_ENC_KEY = "Pg-s_E_n_C_k_e_y";

	// time
	public static final long EXPIRE_RETAINED_TIME = 30/* minute */* 60 * 1000;
	public static final String PREF_LAST_API_TIME = "pref_last_api_time";

	// URL list
	public static final String API_DEVICE_CERT = "deviceCert.m";
	public static final String API_COLLECT_LOG = "collectLog.m";
	public static final String API_LOGIN_PMS = "loginPms.m";
	public static final String API_NEW_MSG = "newMsg.m";
	public static final String API_READ_MSG = "readMsg.m";
	public static final String API_CLICK_MSG = "clickMsg.m";
	public static final String API_SET_CONFIG = "setConfig.m";
	public static final String API_LOGOUT_PMS = "logoutPms.m";
	public static final String API_SET_MSGKIND_PMS = "setMsgKind.m";
	public static final String API_REALTIME_PUSH = "realtimePush.m";
	public static final String API_GET_SIGNKEY = "getSignKey.m";

	// request params(json) key
	public static final String KEY_API_DEFAULT = "d";
	public static final String KEY_APP_USER_ID = "id";
	public static final String KEY_ENC_PARAM = "data";

	// result params(json) key
	public static final String KEY_API_CODE = "code";
	public static final String KEY_API_MSG = "msg";

	// result code (server)
	public static final String CODE_SUCCESS = "000";
	public static final String CODE_NULL_PARAM = "100";
	public static final String CODE_WRONG_SIZE_PARAM = "101";
	public static final String CODE_WRONG_PARAM = "102";
	public static final String CODE_DECRPYT_FAIL = "103";
	public static final String CODE_PARSING_JSON_ERROR = "104";
	public static final String CODE_WRONG_SESSION = "105";
	public static final String CODE_ENCRYPT_ERROR = "106";
	public static final String CODE_API_INVALID = "109";
	public static final String CODE_INNER_ERROR = "200";
	public static final String CODE_EXTRA_ERROR = "201";

	// result code (client)
	public static final String CODE_SESSION_EXPIRED = "901";
	public static final String CODE_CONNECTION_ERROR = "902";
	public static final String CODE_NOT_RESPONSE = "903";
	public static final String CODE_URL_IS_NULL = "904";
	public static final String CODE_PARAMS_IS_NULL = "905";
	public static final String CODE_LICENSE_PENDING = "906";
	public static final String CODE_LICENSE_EXPIRED = "907";

	// device cert
	public static final String NO_TOKEN = "noToken";

	// new msg type
	public static final String TYPE_PREV = "P";
	public static final String TYPE_NEXT = "N";

	// auth status
	public static final String DEVICECERT_PENDING = "devicecert_pending";
	public static final String DEVICECERT_PROGRESS = "devicecert_progress";
	public static final String DEVICECERT_FAIL = "devicecert_fail";
	public static final String DEVICECERT_COMPLETE = "devicecert_complete";
	// [end] api

	// [start] Property key
	// PMS APP KEY & API SERVER URL
	public static final String PRO_GCM_PROJECT_ID = "gcm_project_id";
	public static final String PRO_APP_KEY = "app_key";
	public static final String PRO_API_SERVER_URL = "api_server_url";
	public static final String PRE_DOWNLOAD_SERVER_URL = "download_server_url";

	// MQTT SETTING
	public static final String PRO_MQTT_FLAG = "mqtt_flag";
	public static final String PRO_MQTT_SERVER_KEEPALIVE = "mqtt_server_keepalive";
	public static final String PRO_MQTT_SERVER_URL_SSL = "mqtt_server_url_ssl";
	public static final String PRO_MQTT_SERVER_URL_TCP = "mqtt_server_url_tcp";

	// DEBUG MODE
	public static final String PRO_DEBUG_FALG = "debug_flag";
	public static final String PRO_DEBUG_TAG = "debug_tag";
	public static final String PRO_DEBUG_LOG_FLAG = "debug_log_falg";

	// PMS SETTING
	public static final String PRO_SCREEN_WAKEUP_FLAG = "screen_wakeup_flag";
	public static final String PRO_PUSH_POPUP_SHOWING_TIME = "push_popup_showing_time";
	public static final String PRO_PUSH_POPUP_SHOWINT_FLAG = "push_popup_showing_flag";
	public static final String PRO_NOTI_RECEIVER = "noti_receiver";
	public static final String PRO_PUSH_POPUP_ACTIVITY = "push_popup_activity";
	// [end] Property key

	// [start] DB Key
	public static final String DB_GCM_TOKEN = "registration_id";
	public static final String DB_CUST_ID = "cust_id";
	public static final String DB_LOGINED_CUST_ID = "logined_cust_id";
	public static final String DB_MAX_USER_MSG_ID = "max_user_msg_id";

	public static final String DB_MSG_FLAG = "msg_flag";
	public static final String DB_NOTI_FLAG = "noti_flag";
	public static final String DB_API_LOG_FLAG = "api_log_flag";
	public static final String DB_PRIVATE_LOG_FLAG = "private_log_flag";
	public static final String DB_PRIVATE_FLAG = "private_flag";
	public static final String DB_ALERT_FLAG = "alert_flag";
	public static final String DB_RING_FLAG = "ring_flag";
	public static final String DB_VIBE_FLAG = "vibe_flag";
	public static final String DB_LICENSE_FLAG = "license_flag";
	public static final String DB_SDK_LOCK_FLAG = "sdk_lock_flag";
	public static final String DB_SSL_SIGN_KEY = "ssl_sign_key";
	public static final String DB_SSL_SIGN_PASS = "ssl_sign_pass";

	public static final String DB_PRIVATE_PROTOCOL = "private_protocol";
	public static final String DB_ISPOPUP_ACTIVITY = "ispopup_activity";
	public static final String DB_NOTIORPOPUP_SETTING = "notiorpopup_setting";

	public static final String DB_ONEDAY_LOG = "oneday_log";
	public static final String DB_YESTERDAY = "yesterday";
	public static final String DB_LICENSE_YESTERDAY = "license_yesterday";

	public static final String DB_APP_USER_ID = "app_user_id";
	public static final String DB_ENC_KEY = "enc_key";
	public static final String DB_APP_KEY_CHECK = "app_key_check";
	public static final String DB_APP_KEY = "app_key";
	public static final String DB_API_SERVER_CHECK = "api_server_check";
	public static final String DB_SERVER_URL = "server_url";
	public static final String DB_MQTT_SERVER_CHECK = "mqtt_server_check";
	public static final String DB_MQTT_SERVER_SSL_URL = "mqtt_server_ssl_url";
	public static final String DB_MQTT_SERVER_TCP_URL = "mqtt_server_tcp_url";
	public static final String PREF_UUID = "uuid";
	public static final String DB_UUID = "uuid";

	public static final String DB_USE_BIGTEXT = "use_bigtext";

	public static final String DB_NOTI_CHANNEL_ID = "noti_channel_id";
	// [end] DB Key

	// [start] Property key
	public static final String PRO_ENABLE_TMS_UUID = "enable_PmsUUID";

	public static final String PREF_NOTI_SOUND = "noti_sound";

	// [end] Property key

	public static final String META_DATA_NOTI_O_BADGE ="PMS_NOTI_O_BADGE";
    public static final String PRO_PUSH_RECEIVER_ACTION = "push_receiver_action";
	public static final String PRO_PUSH_RECEIVER_CLASS = "push_receiver_class";
    public static final String PRO_NOTI_RECEIVER_ACTION = "noti_receiver_action";
	public static final String PRO_NOTI_RECEIVER_CLASS = "noti_receiver_class";
}

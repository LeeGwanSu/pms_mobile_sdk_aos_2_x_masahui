package com.pms.sdk.common.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.PowerManager;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.PMS;

/**
 * 
 * @author erzisk
 * @since 2013.06.26
 */
public class PMSUtil implements IPMSConsts {

	/**
	 * MQTT Server URL
	 * 
	 * @param context
	 * @return
	 */
	public static void setMQTTServerUrl (final Context context, String... serverUrl) {
		DataKeyUtil.setDBKey(context, DB_MQTT_SERVER_SSL_URL, serverUrl[0]);
		DataKeyUtil.setDBKey(context, DB_MQTT_SERVER_TCP_URL, serverUrl[1]);
	}

	public static String getMQTTServerUrl (final Context context) {
		try {
			String mqttServerUrl = "";
			String mqttProtocol = getPrivateProtocol(context);
			if (FLAG_Y.equals(DataKeyUtil.getDBKey(context, DB_MQTT_SERVER_CHECK))) {
				if (PROTOCOL_SSL.equals(mqttProtocol)) {
					mqttServerUrl = DataKeyUtil.getDBKey(context, DB_MQTT_SERVER_SSL_URL);
				} else {
					mqttServerUrl = DataKeyUtil.getDBKey(context, DB_MQTT_SERVER_TCP_URL);
				}
			} else {
				if (PROTOCOL_SSL.equals(mqttProtocol)) {
					mqttServerUrl = ProPertiesFileUtil.getString(context, PRO_MQTT_SERVER_URL_SSL);
				} else {
					mqttServerUrl = ProPertiesFileUtil.getString(context, PRO_MQTT_SERVER_URL_TCP);
				}
			}
			return mqttServerUrl == null || mqttServerUrl.equals("") ? "" : mqttServerUrl;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * Application Key
	 * 
	 * @param context
	 * @return
	 */
	public static void setApplicationKey (final Context context, String key) {
		DataKeyUtil.setDBKey(context, DB_APP_KEY, key);
	}

	public static String getApplicationKey (final Context context) {
		try {
			String appKey = "";
			if (FLAG_Y.equals(DataKeyUtil.getDBKey(context, DB_APP_KEY_CHECK))) {
				appKey = DataKeyUtil.getDBKey(context, DB_APP_KEY);
			} else {
				appKey = ProPertiesFileUtil.getString(context, PRO_APP_KEY);
			}
			return appKey;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * set server url
	 * 
	 * @param context
	 * @param serverUrl
	 */
	public static void setServerUrl (final Context context, String serverUrl) {
		DataKeyUtil.setDBKey(context, DB_SERVER_URL, serverUrl);
	}

	public static String getServerUrl (final Context context) {
		try {
			String serverUrl = "";
			if (FLAG_Y.equals(DataKeyUtil.getDBKey(context, DB_API_SERVER_CHECK))) {
				serverUrl = DataKeyUtil.getDBKey(context, DB_SERVER_URL);
			} else {
				serverUrl = ProPertiesFileUtil.getString(context, PRO_API_SERVER_URL);
			}

			return serverUrl == null || serverUrl.equals("") ? "" : serverUrl;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * GCM Token
	 * 
	 * @param context
	 * @param gcmToken
	 */
	public static void setGCMToken (final Context context, String gcmToken) {
		DataKeyUtil.setDBKey(context, DB_GCM_TOKEN, gcmToken);
	}

	public static String getGCMToken (final Context context) {
		return DataKeyUtil.getDBKey(context, DB_GCM_TOKEN);
	}

	/**
	 * get createtoken flag
	 *
	 * @param context
	 * @return
	 */
	public static String getEnableUUIDFlag(final Context context) {
		try {
			String uuidFlag = (String) context.getPackageManager().getApplicationInfo(
					context.getPackageName(), PackageManager.GET_META_DATA).metaData
					.get("PMS_ENABLE_UUID");
			return (uuidFlag == null) ? FLAG_Y : uuidFlag;
		} catch (Exception e) {
			e.printStackTrace();
			return FLAG_N;
		}
	}

	/**
	 * set UUID
	 *
	 * @param context
	 * @param gcmToken
	 */
	public static void setUUID (final Context context, String gcmToken) {
		// 20170811 fixed by hklim
		// UUID 저장 시에 Shared Preference 를 사용하지 않고 DB 사용 하도록 수정됨
		DataKeyUtil.setDBKey(context, DB_UUID, gcmToken);
	}

	/**
	 * get UUID
	 *
	 * @param context
	 * @return
	 */
	public static String getUUID (final Context context) {
		String strReturn = DataKeyUtil.getDBKey(context, DB_UUID);
		return strReturn;
	}


	/**
	 * Encrypt Key
	 * 
	 * @param context
	 * @param encKey
	 */
	public static void setEncKey (final Context context, String encKey) {
		DataKeyUtil.setDBKey(context, DB_ENC_KEY, encKey);
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_ENC_KEY, encKey);
	}

	public static String getEncKey (final Context context) {
		return DataKeyUtil.getDBKey(context, DB_ENC_KEY);
	}

	/**
	 * App User Id
	 * 
	 * @param context
	 * @param appUserId
	 */
	public static void setAppUserId (final Context context, String appUserId) {
		DataKeyUtil.setDBKey(context, DB_APP_USER_ID, appUserId);
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_APP_USER_ID, appUserId);
	}

	public static String getAppUserId (final Context context) {
		return DataKeyUtil.getDBKey(context, DB_APP_USER_ID);
	}

	/**
	 * Cust Id
	 * 
	 * @param context
	 * @param custId
	 */
	public static void setCustId (final Context context, String custId) {
		DataKeyUtil.setDBKey(context, DB_CUST_ID, custId);
	}

	public static String getCustId (final Context context) {
		return DataKeyUtil.getDBKey(context, DB_CUST_ID);
	}

	/**
	 * IS Popup Activity
	 * 
	 * @param context
	 * @param state
	 */
	public static void setPopupActivity (Context context, Boolean state) {
		DataKeyUtil.setDBKey(context, DB_ISPOPUP_ACTIVITY, state ? "Y" : "N");
	}

	public static Boolean getPopupActivity (Context context) {
		return (FLAG_Y.equals(DataKeyUtil.getDBKey(context, DB_ISPOPUP_ACTIVITY))) ? true : false;
	}

	/**
	 * Noti or Popup
	 * 
	 * @param context
	 * @param isNotiPopup
	 */
	public static void setNotiOrPopup (Context context, Boolean isNotiPopup) {
		DataKeyUtil.setDBKey(context, DB_NOTIORPOPUP_SETTING, isNotiPopup ? "Y" : "N");
	}

	public static Boolean getNotiOrPopup (Context context) {
		return (FLAG_Y.equals(DataKeyUtil.getDBKey(context, DB_NOTIORPOPUP_SETTING))) ? true : false;
	}

	/**
	 * DeviceCert Status
	 * 
	 * @param context
	 * @param status
	 */
	public static void setDeviceCertStatus (final Context context, String status) {
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_DEVICECERT_STATUS, status);
	}

	public static String getDeviceCertStatus (final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_DEVICECERT_STATUS);
	}

	/**
	 * API License Flag
	 * 
	 * @param context
	 * @param flag
	 */
	public static void setLicenseFlag (final Context context, String flag) {
		DataKeyUtil.setDBKey(context, DB_LICENSE_FLAG, flag);
	}

	public static String getLicenseFlag (final Context context) {
		return DataKeyUtil.getDBKey(context, DB_LICENSE_FLAG);
	}

	/**
	 * SDK Lock Flag
	 * 
	 * @param context
	 * @param flag
	 */
	public static void setSDKLockFlag (final Context context, String flag) {
		DataKeyUtil.setDBKey(context, DB_SDK_LOCK_FLAG, flag);
	}

	public static String getSDKLockFlag (final Context context) {
		return DataKeyUtil.getDBKey(context, DB_SDK_LOCK_FLAG);
	}

	/**
	 * GCM Project ID
	 * 
	 * @param context
	 */
	public static String getGCMProjectId (final Context context) {
		return ProPertiesFileUtil.getString(context, PRO_GCM_PROJECT_ID);
	}

	/**
	 * MQTT FLAG
	 * 
	 * @param context
	 */
	public static String getMQTTFlag (final Context context) {
		try {
			String mqttFlag = ProPertiesFileUtil.getString(context, PRO_MQTT_FLAG);
			return mqttFlag == null || mqttFlag.equals("") ? FLAG_N : mqttFlag;
		} catch (Exception e) {
			e.printStackTrace();
			return FLAG_N;
		}
	}

	/**
	 * get private flag
	 * 
	 * @param context
	 * @return
	 */
	public static String getPrivateFlag (final Context context) {
		return DataKeyUtil.getDBKey(context, DB_PRIVATE_FLAG);

	}

	/**
	 * get Private Log Flag
	 * 
	 * @param context
	 * @return
	 */
	public static String getPrivateLogFlag (final Context context) {
		return DataKeyUtil.getDBKey(context, DB_PRIVATE_LOG_FLAG);
	}

	/**
	 * get Private Log Flag
	 * 
	 * @param context
	 * @return
	 */
	public static String getPrivateProtocol (final Context context) {
		return DataKeyUtil.getDBKey(context, DB_PRIVATE_PROTOCOL);
	}

	/**
	 * get mqtt server url
	 * 
	 * @param context
	 * @return
	 */
	public static String getMQTTServerKeepAlive (final Context context) {
		try {
			String keepalive = ProPertiesFileUtil.getString(context, PRO_MQTT_SERVER_KEEPALIVE);
			return keepalive == null || keepalive.equals("") ? "400" : keepalive;
		} catch (Exception e) {
			e.printStackTrace();
			return "400";
		}
	}

	public static String getBigNotiContextMsg (final Context context) {
		try {
			return (String) context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
					.get("PMS_NOTI_CONTENT");
		} catch (NameNotFoundException e) {
			e.printStackTrace();
			return "";
		}
	}

	public static int getIconId (final Context context) {
		try {
			int iconResId = (Integer) context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
					.get("PMS_SET_ICON");

			return iconResId;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public static int getLargeIconId (final Context context) {
		try {
			int iconResId = (Integer) context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
					.get("PMS_SET_LARGE_ICON");

			return iconResId;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public static int getNotiSound (final Context context) {
		try {
			int iconResId = (Integer) context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
					.get("PMS_SET_NOTI_SOUND");
			return iconResId;
		} catch (Exception e) {
			return 0;
		}
	}

	/**
	 * get Link From html
	 * 
	 * @param htmlStr
	 * @return
	 */
	@SuppressLint("UseSparseArrays")
	public static Map<Integer, String> getLinkFromHtml (String htmlStr) {
		Map<Integer, String> map = new HashMap<Integer, String>();
		Pattern p = Pattern.compile("<a\\s*href\\s*=\\s*([\"']*)([^\"'>\\s]+)\\1(.*?)>(.*?)</a>");
		Matcher m = p.matcher(htmlStr);
		for (int i = 1; m.find(); i++) {
			map.put(i, m.group(2));
		}
		return map;
	}

	/**
	 * replaceLink
	 * 
	 * @param htmlStr
	 * @return
	 */
	public static String replaceLink (String htmlStr) {
		StringBuffer s = new StringBuffer();
		Pattern p = Pattern.compile("<a\\s*href\\s*=\\s*([\"']*)([^\"'>\\s]+)\\1(.*?)>(.*?)</a>");
		Matcher m = p.matcher(htmlStr);
		for (int i = 1; m.find(); i++) {
			m.appendReplacement(s, m.group(0).replace(m.group(2), "click://" + i));
		}
		m.appendTail(s);
		return s.toString();
	}

	/**
	 * find top activity
	 * 
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static String[] findTopActivity (Context c) {
		String[] top = new String[2];
		ActivityManager activityManager = (ActivityManager) c.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> info;
		info = activityManager.getRunningTasks(1);
		for (Iterator<RunningTaskInfo> iterator = info.iterator(); iterator.hasNext();) {
			RunningTaskInfo runningTaskInfo = iterator.next();

			top[0] = runningTaskInfo.topActivity.getPackageName();
			top[1] = runningTaskInfo.topActivity.getClassName();
		}

		return top;
	}

	/**
	 * runnedApp 현재 어플리케이션이 실행중인지 여부
	 * 
	 * @return
	 */
	public static boolean isRunnedApp (Context c) {
		if (findTopActivity(c)[0].equals(c.getPackageName())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * is screen on
	 * 
	 * @param c
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static boolean isScreenOn (Context c) {
		PowerManager pm;
		try {
			pm = (PowerManager) c.getSystemService(Context.POWER_SERVICE);
			return pm.isScreenOn();
		} catch (Exception e) {
			return false;
		} finally {
			PMS.clear();
		}
	}

	/**
	 * Make Read Param
	 * 
	 * @param msgId
	 * @return
	 */
	public static JSONObject getReadParam (String msgId) {
		JSONObject read = new JSONObject();
		try {
			read.put("msgId", msgId);
			read.put("workday", DateUtil.getNowDate());
			return read;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * array to preferences
	 * 
	 * @param obj
	 */
	public static void arrayToPrefs (Context c, String key, Object obj) {
		Prefs prefs = new Prefs(c);
		String arrayString = prefs.getString(key);
		JSONArray array = null;
		try {
			if ("".equals(arrayString)) {
				array = new JSONArray();
			} else {
				array = new JSONArray(arrayString);
			}
			array.put(obj);
			prefs.putString(key, array.toString());
		} catch (Exception e) {
			// e.printStackTrace();
		}
	}

	/**
	 * array from preferences
	 * 
	 * @param c
	 * @param key
	 * @return
	 */
	public static JSONArray arrayFromPrefs (Context c, String key) {
		Prefs prefs = new Prefs(c);
		String arrayString = prefs.getString(key);
		JSONArray array = new JSONArray();
		try {
			array = new JSONArray(arrayString);
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return array;
	}

	public static int getTargetVersion(final Context context) {
		try {
			int numTargetVersion = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).targetSdkVersion;
			return numTargetVersion;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public static String getApplicationName(Context context) {
		PackageManager packageManager = context.getPackageManager();
		ApplicationInfo applicationInfo = null;
		try {
			applicationInfo = packageManager.getApplicationInfo(context.getApplicationInfo().packageName, 0);
		}
		catch (final NameNotFoundException e) {
		}
		finally {
			return (String) (applicationInfo != null ? packageManager.getApplicationLabel(applicationInfo) : context.getPackageName());
		}
	}
}
